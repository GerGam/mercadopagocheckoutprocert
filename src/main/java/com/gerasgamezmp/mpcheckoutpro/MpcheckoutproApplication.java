package com.gerasgamezmp.mpcheckoutpro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MpcheckoutproApplication {

	public static void main(String[] args) {
		SpringApplication.run(MpcheckoutproApplication.class, args);
	}

}
