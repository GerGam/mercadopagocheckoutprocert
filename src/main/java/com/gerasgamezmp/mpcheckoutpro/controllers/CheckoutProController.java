package com.gerasgamezmp.mpcheckoutpro.controllers;


import com.gerasgamezmp.mpcheckoutpro.model.WebHook;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;


@RestController
@Slf4j
public class CheckoutProController {

    @RequestMapping(value="/createPreference", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, String> createPreference(){
        HashMap<String, String> resp = new HashMap<>();

        log.info("Logging preference creation {}", "variable");
        resp.put("ResponseCode", "200");
        resp.put("ResponseText", "preference created!!");
        return resp;

    }

    @RequestMapping(value="/receiveWebHooks", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, String> receiveWebHooks(@RequestBody WebHook req){
        HashMap<String, String> resp = new HashMap<>();

        log.info("Logging receiving webhooks id: {}", req.getId());
        log.info("Logging receiving webhooks live_mode: {}", req.isLiveMode());
        log.info("Logging receiving webhooks type: {}", req.getType());
        log.info("Logging receiving webhooks date_created: {}", req.getDateCreated());
        log.info("Logging receiving webhooks application_id: {}", req.getApplicationId());
        log.info("Logging receiving webhooks user_id: {}", req.getUserId());
        log.info("Logging receiving webhooks version: {}", req.getVersion());
        log.info("Logging receiving webhooks api_version: {}", req.getApiVersion());
        log.info("Logging receiving webhooks action: {}", req.getAction());
        log.info("Logging receiving webhooks data.id: {}", req.getData().getId());
        resp.put("ResponseCode", "200");
        resp.put("ResponseText", "webhook received!!");
        return resp;

    }

}
