package com.gerasgamezmp.mpcheckoutpro.model;

import java.io.Serializable;

public class Data implements Serializable {

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
