package com.gerasgamezmp.mpcheckoutpro.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class WebHook implements Serializable {

    private int id;

    @JsonProperty("live_mode")
    private boolean liveMode;

    private String type;

    @JsonProperty("date_created")
    private String dateCreated;

    @JsonProperty("application_id")
    private int applicationId;

    @JsonProperty("user_id")
    private int userId;

    private String version;

    @JsonProperty("api_version")
    private String apiVersion;

    private String action;

    private Data data;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isLiveMode() {
        return liveMode;
    }

    public void setLiveMode(boolean liveMode) {
        this.liveMode = liveMode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public int getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(int applicationId) {
        this.applicationId = applicationId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getApiVersion() {
        return apiVersion;
    }

    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
